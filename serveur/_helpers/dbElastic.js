const config = require('configElastic.json');
const elasticsearch = require('elasticsearch');

const client = new elasticsearch.Client({
    host: config.connectionString,
    log:'trace',
    apiVersion:'7.4'
});

module.exports = {
    client
};
