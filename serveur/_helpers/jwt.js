const expressJwt = require('express-jwt');
const config = require('configUsers.json');
const userService = require('../users/user.service');

module.exports = jwt;

function jwt() {
    const secret = config.secret;
    return expressJwt({ secret, isRevoked }).unless({
        path: [
            // public routes that don't require authentication
            '/users/authenticate',
            '/users/register',
            '/reviews/doFilter',
            '/reviews/searchReview',
            '/reviews/readLastTen',
            '/reviews/readRestaurantReviews',
            '/restaurants/readLastTen',
            '/restaurants/searchRestaurant',
            '/restaurants/readOne',
            '/restaurants/doFilter'

        ]
    });
}

async function isRevoked(req, payload, done) {
    const user = await userService.getById(payload.sub);

    // revoke token if user no longer exists
    if (!user) {
        return done(null, true);
    }

    done();
};
