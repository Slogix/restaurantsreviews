const express = require('express');
const router = express.Router();
const restaurantService = require('./restaurants.service');

router.post('/createRestaurant', createRestaurant);
router.get('/readLastTen', readLastTen);
router.post('/readOne', readOne);
router.post('/searchRestaurant', searchRestaurant);
router.post('/doFilter', doFilter);

module.exports = router;

function createRestaurant(req, res, next) {
    restaurantService.create(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function readLastTen(req, res, next){
    restaurantService.readLastTen({})
        .then(( restaurants) => res.json(restaurants))
        .catch(err => next(err))
}

function readOne(req, res, next) {
    restaurantService.readOne(req.body)
        .then(( restaurant) => res.json(restaurant))
        .catch(err => next(err))
}

function searchRestaurant(req, res, next){
    restaurantService.doSearch(req.body)
        .then((restaurants) => res.json(restaurants))
        .catch(err => next(err))
}

function doFilter(req, res, next){
    restaurantService.doFilter(req.body)
        .then((restaurants) => res.json(restaurants))
        .catch(err => next(err))
}
