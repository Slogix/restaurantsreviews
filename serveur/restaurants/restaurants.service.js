const config = require('configElastic.json');
const client = require('_helpers/dbElastic').client;

module.exports = {
    create,
    readLastTen,
    doSearch,
    readOne,
    doFilter
};

async function readLastTen() {
    let restaurants = [];
    const response = await client.search({
        index: 'restaurant',
        size: 10,
        sort: ['createdDate:desc']
    });
    await response.hits.hits.forEach( restaurant =>{
        restaurants.push({
            id: restaurant._id,
            name: restaurant._source.name,
            city: restaurant._source.city,
            street: restaurant._source.street,
            createdBy: restaurant._source.createdBy,
            createdDate: restaurant._source.createdDate,
            lat: restaurant._source.location.lat,
            long: restaurant._source.location.lon,
            nbReviews: restaurant._source.nbReviews,
            score: restaurant._source.score,
            price: restaurant._source.price,
            mood: restaurant._source.mood,
            typeCook: restaurant._source.typeCook
        });
    });
    return restaurants;
}

async function create({name, phoneNumber, city, street, username, date, lat, long}) {
    await client.index({
        index: 'restaurant',
        body: {
            name: name,
            phoneNumber: phoneNumber,
            city: city,
            street: street,
            createdDate : date,
            createdBy : username,
            location:{
                lat: lat,
                lon: long
            }
        }
    });
    await client.indices.refresh({ index: 'restaurant' })
}

async function readOne({id}){
    const response = await client.search({
        index: 'restaurant',
        body: {
            query: {
                match: { _id: id},
            }
        }
    });
    if (response.hits.total.value === 1) {
        let restaurant = response.hits.hits[0];
        return {
            id: restaurant._id,
            name: restaurant._source.name,
            phoneNumber: restaurant._source.phoneNumber,
            city: restaurant._source.city,
            street: restaurant._source.street,
            createdBy: restaurant._source.createdBy,
            createdDate: restaurant._source.createdDate,
            lat: restaurant._source.location.lat,
            long: restaurant._source.location.lon,
            nbReviews: restaurant._source.nbReviews,
            score: restaurant._source.score,
            price: restaurant._source.price,
            mood: restaurant._source.mood,
            typeCook: restaurant._source.typeCook
        };
    }else {
        return {}
    }
}

async function doSearch(search){
    let restaurants = [];
    const response = await client.search({
        index: 'restaurant',
        body: {
            query: {
                multi_match: {
                    query: search.q,
                    fields: [ "name", "city", "street", "createdBy" ],
                    fuzziness: 1
                }
            }
        }

    });
    await response.hits.hits.forEach( restaurant =>{
        restaurants.push({
            id: restaurant._id,
            name: restaurant._source.name,
            city: restaurant._source.city,
            street: restaurant._source.street,
            createdBy: restaurant._source.createdBy,
            createdDate: restaurant._source.createdDate,
            lat: restaurant._source.location.lat,
            long: restaurant._source.location.lon,
            nbReviews: restaurant._source.nbReviews,
            score: restaurant._source.score,
            price: restaurant._source.price,
            mood: restaurant._source.mood,
            typeCook: restaurant._source.typeCook
        });
    });
    return restaurants;
}

async function doFilter({q, typeFood, note, ambiance, price, distance, long, lat}){
    let restaurants = [];
    let filter = [
        { 'range': { score : { "gte": note}}},
        { 'range': { price : { "gte": price}}}
    ];
    if (distance && lat !== undefined && long !== undefined){
        filter.push({ 'geo_distance':
                {
                    'distance': distance+'km',
                    'location': {
                        'lat': lat,
                        'lon': long
                    }
                }})
    }
    if (typeFood !== ""){
        filter.push({ 'match': { "typeCook": typeFood}})
    }
    if (ambiance !== undefined ){
        filter.push({ 'match': { "mood": ambiance}})
    }
    const response = await client.search({
        index: 'restaurant',
        body: {
            query: {
                bool: {
                    must:[{
                        multi_match: {
                            query: q,
                            fields: ["name", "city", "street", "createdBy"],
                            fuzziness: 1
                        }
                    }],
                    filter: filter
                }
            }
        }
    });
    await response.hits.hits.forEach( restaurant =>{
        restaurants.push({
            id: restaurant._id,
            name: restaurant._source.name,
            city: restaurant._source.city,
            street: restaurant._source.street,
            createdBy: restaurant._source.createdBy,
            createdDate: restaurant._source.createdDate,
            lat: restaurant._source.location.lat,
            long: restaurant._source.location.lon,
            nbReviews: restaurant._source.nbReviews,
            score: restaurant._source.score,
            price: restaurant._source.price,
            mood: restaurant._source.mood,
            typeCook: restaurant._source.typeCook
        });
    });
    return restaurants
}
