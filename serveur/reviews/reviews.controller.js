const express = require('express');
const router = express.Router();
const reviewService = require('./reviews.service');

router.post('/createReview', createReview);
router.post('/readRestaurantReviews', readRestaurantReviews);
router.post('/searchReview', searchReview);
router.post('/doFilter', doFilter);
router.get('/readLastTen', readLastTen);
router.post('/searchReviewRestaurant', searchReviewRestaurant);

module.exports = router;

function createReview(req, res, next){
    reviewService.create(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function readRestaurantReviews(req, res, next){
    reviewService.readRestaurantReviews(req.body)
        .then((reviews) => res.json(reviews))
        .catch(err => next(err));
}

function searchReviewRestaurant(req, res, next){
    reviewService.doSearchRestaurant(req.body)
        .then((reviews) => res.json(reviews))
        .catch(err => next(err));
}

function readLastTen(req, res, next){
    reviewService.readLastTen()
        .then((reviews) => res.json(reviews))
        .catch(err => next(err));
}

function searchReview(req, res, next){
    reviewService.doSearch(req.body)
        .then((reviews) => res.json(reviews))
        .catch( err => next(err));
}

function doFilter(req, res, next){
    reviewService.doFilter(req.body)
        .then((reviews) => res.json(reviews))
        .catch( err => next(err));
}
