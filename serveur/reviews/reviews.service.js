//const config = require('configElastic.json');
const client = require('_helpers/dbElastic').client;
const restaurantService = require('../restaurants/restaurants.service');

module.exports = {
    create,
    readLastTen,
    doSearch,
    doSearchRestaurant,
    readRestaurantReviews,
    doFilter
};

async function readLastTen() {
    let reviews = [];
    const response = await client.search({
        index: 'reviews',
        size: 10,
        sort: ['createdDate:desc']
    });
    for ( const review of response.hits.hits ){
        let restaurant = await restaurantService.readOne({id : review._source.idRestaurant});
        reviews.push({
            idRestaurant: review._source.idRestaurant,
            restaurantName: restaurant.name,
            title: review._source.title,
            avis: review._source.avis,
            score: review._source.score,
            createdBy: review._source.createdBy,
            createdDate: review._source.createdDate,
            typeFood: review._source.cookScore,
            price: review._source.price,
            mood: review._source.mood
        });
    }
    return reviews;
}
async function create({createdBy, restaurantName, idRestaurant, title, avis, score, createdDate, price, noteService, noteCuisine, mood, typeFood}) {
    await client.bulk({
        body: [
            { index: { _index: 'reviews' }},
            {
                idRestaurant: idRestaurant,
                restaurantName: restaurantName,
                title: title,
                avis: avis,
                score : score,
                createdBy : createdBy,
                createdDate: createdDate,
                price: price,
                serviceScore: noteService,
                cookScore: noteCuisine,
                mood: mood,
                typeCook: typeFood
            },
            { update: { _index: 'restaurant', _id: idRestaurant }},
            { script: { source: "if (!ctx._source.containsKey('nbReviews'))" +
                                "{ ctx._source.nbReviews = 1 }" +
                                "else" +
                                "{ ctx._source.nbReviews += 1 }" +
                                "if (!ctx._source.containsKey('score'))" +
                                "{ ctx._source.score = params.score }" +
                                "else" +
                                "{ ctx._source.score = (ctx._source.score*(ctx._source.nbReviews-1) + params.score)/ctx._source.nbReviews }" +
                                "if (!ctx._source.containsKey('price'))" +
                                "{ ctx._source.price = params.price }" +
                                "else" +
                                "{ ctx._source.price = (ctx._source.price*(ctx._source.nbReviews-1)+ params.price)/ctx._source.nbReviews }" +
                                "if (!ctx._source.containsKey('mood'))" +
                                "{ ctx._source.mood = params.mood }" +
                                "else" +
                                "{ ctx._source.mood +=' '+params.mood  }" +
                                "if (!ctx._source.containsKey('typeCook'))" +
                                "{ ctx._source.typeCook = params.typeCook }" +
                                "else" +
                                "{ ctx._source.typeCook +=' '+params.typeCook  }",
                        params :{
                            score: parseFloat(score),
                            price: parseFloat(price),
                            cookScore: parseFloat(noteCuisine),
                            typeCook: typeFood,
                            mood: mood
                        }
                }
            }
        ]
    });
    await client.indices.refresh({ index: 'reviews' });
    await client.indices.refresh({ index: 'restaurant' });
}
async function readRestaurantReviews({id}){
    let reviews = [];
    const response = await client.search({
        index: 'reviews',
        body: {
            query: {
                match: { idRestaurant: id},
            }
        }
    });
    await response.hits.hits.forEach( review =>{
        reviews.push(review._source);
    });
    return reviews;
}

async function doSearch(search){

    let reviews = [];
    const response = await client.search({
        index: 'reviews',
        body: {
            query: {
                multi_match: {
                    query: search.q,
                    fields: [ "title", "avis", "createdBy", "restaurantName"],
                    fuzziness: 1
                }
            }
        }

    });
    for ( const review of response.hits.hits ){
        let restaurant = await restaurantService.readOne({id : review._source.idRestaurant});
        reviews.push({
            idRestaurant: review._source.idRestaurant,
            restaurantName: restaurant.name,
            title: review._source.title,
            avis: review._source.avis,
            score: review._source.score,
            createdBy: review._source.createdBy,
            createdDate: review._source.createdDate,
            typeFood: review._source.cookScore,
            price: review._source.price,
            mood: review._source.mood
        });
    }
    return reviews;
}

async function doSearchRestaurant({q, id}){

    let reviews = [];
    const response = await client.search({
        index: 'reviews',
        body: {
            query: {
                bool: {
                    should:[{
                        multi_match: {
                            query: q,
                            fields: ["title", "avis", "createdBy"],
                            fuzziness: 1
                        }
                    }],
                    filter: { 'match': { "idRestaurant": id}}
                }
            }
        }

    });
    for ( const review of response.hits.hits ){
        let restaurant = await restaurantService.readOne({id : review._source.idRestaurant});
        reviews.push({
            idRestaurant: review._source.idRestaurant,
            restaurantName: restaurant.name,
            title: review._source.title,
            avis: review._source.avis,
            score: review._source.score,
            createdBy: review._source.createdBy,
            createdDate: review._source.createdDate,
            typeFood: review._source.cookScore,
            price: review._source.price,
            mood: review._source.mood
        });
    }
    return reviews;
}

async function doFilter({q, dateDebut, dateFin, note, ambiance, price}){
    let reviews = [];
    let filter = [
        { 'range': { score : { "gte": note}}},
        { 'range': { price : { "gte": price}}}
    ];
    if (ambiance !== undefined ){
        filter.push({ 'term': { "mood": ambiance}})
    }
    if (dateDebut !== null ){
        filter.push({ 'range': { "createdDate" : {"gte": dateDebut}}})
    }
    if (dateFin !== null ){
        filter.push({ 'range': { "createdDate" : {"lte": dateFin}}})
    }
    const response = await client.search({
        index: 'reviews',
        body: {
            query: {
                bool: {
                    should:[{
                        multi_match: {
                            query: q,
                            fields: ["title", "avis", "createdBy"],
                            fuzziness: 1
                        }
                    }],
                    filter: filter
                }
            }
        }
    });
    for ( const review of response.hits.hits ){
        let restaurant = await restaurantService.readOne({id : review._source.idRestaurant});
        reviews.push({
            idRestaurant: review._source.idRestaurant,
            restaurantName: restaurant.name,
            title: review._source.title,
            avis: review._source.avis,
            score: review._source.score,
            createdBy: review._source.createdBy,
            createdDate: review._source.createdDate,
            typeFood: review._source.cookScore,
            price: review._source.price,
            mood: review._source.mood
        });
    }
    return reviews;
}
