import {Component, Input, OnInit} from '@angular/core';
import localeFr from '@angular/common/locales/fr';
import {Restaurant} from '../_models';


@Component(
  {selector: 'app-restaurant-card',
    templateUrl: 'restaurantCard.component.html',
    styleUrls: ['restaurantCard.component.css']})

export class RestaurantCardComponent implements OnInit {
  @Input() data;
  restaurant: Restaurant;
  constructor() {

  }
  ngOnInit() {
    this.restaurant = this.data;
  }
}
