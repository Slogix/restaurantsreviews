import { Component, OnInit, AfterContentInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { User } from '../_models';
import {UserService, AuthenticationService, RestaurantService, ReviewService, SearchBarService} from '../_services';
import { ActivatedRoute, Router} from '@angular/router';

import * as L from 'leaflet';
import {FormControl} from '@angular/forms';

@Component({ templateUrl: 'restaurantDetails.component.html', styleUrls: ['restaurantDetails.css'] })
export class RestaurantDetailsComponent implements OnInit, AfterContentInit {
  currentUser: User;
  id;
  restaurant;
  reviews;
  search;
  reviewsTab = [];
  map;
  osmLayer;
  marker;
  icon = {
    icon: L.icon({
      iconSize: [ 26, 41 ],
      iconAnchor: [ 13, 41 ],
      // specify the path here
      iconUrl: 'https://unpkg.com/leaflet@1.4.0/dist/images/marker-icon.png',
      shadowUrl: 'https://unpkg.com/leaflet@1.4.0/dist/images/marker-shadow.png'
    })
  };
  pageOfItems: Array<any>;
  moodsPercentage = {
    couple : 0,
    famille: 0,
    amis: 0,
    affaires: 0,
    solo: 0
  };
  typesCookPercentage = {
    americain : 0,
    chinois : 0,
    francais : 0,
    italien : 0,
    japonais : 0,
    mexicain : 0,
    sansGluten : 0,
    thailandais : 0,
    vegan : 0,
    vegetarien : 0,
    vietnamiem : 0
  };

  constructor(
    private authenticationService: AuthenticationService,
    private restaurantService: RestaurantService,
    private reviewService: ReviewService,
    private searchBarService: SearchBarService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.currentUser = this.authenticationService.currentUserValue;
  }

  async ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    await this.loadRestaurantById(this.id);
    await this.loadReviewsById(this.id);
  }

  ngAfterContentInit() {

  }

  private loadRestaurantById(id) {
    this.restaurantService.getRestaurantById({ id })
      .pipe(first())
      .subscribe(rest => {
        this.restaurant = rest;
        console.log(this.restaurant);
        if (typeof this.restaurant.mood !== 'undefined') {
          const moods = this.restaurant.mood.split(' ');
          // tslint:disable-next-line:prefer-for-of
          for (let i = 0; i < moods.length; i++) {
            if (moods[i] === 'couple') {
              this.moodsPercentage.couple += 1;
            }
            if (moods[i] === 'famille') {
              this.moodsPercentage.famille += 1;
            }
            if (moods[i] === 'amis') {
              this.moodsPercentage.amis += 1;
            }
            if (moods[i] === 'affaires') {
              this.moodsPercentage.affaires += 1;
            }
            if (moods[i] === 'solo') {
              this.moodsPercentage.solo += 1;
            }
          }
        }

        if (typeof this.restaurant.typeCook !== 'undefined') {
          const typesCook = this.restaurant.typeCook.split(' ');
          // tslint:disable-next-line:prefer-for-of
          for (let i = 0; i < typesCook.length; i++) {
            // console.log(moods.length);
            if (typesCook[i] === 'Américain') {
              this.typesCookPercentage.americain += 1;
            }
            if (typesCook[i] === 'Chinois') {
              this.typesCookPercentage.chinois += 1;
            }
            if (typesCook[i] === 'Français') {
              this.typesCookPercentage.francais += 1;
            }
            if (typesCook[i] === 'Italien') {
              this.typesCookPercentage.italien += 1;
            }
            if (typesCook[i] === 'Japonais') {
              this.typesCookPercentage.japonais += 1;
            }
            if (typesCook[i] === 'Mexicain') {
              this.typesCookPercentage.mexicain += 1;
            }
            if (typesCook[i] === 'Sans Gluten') {
              this.typesCookPercentage.sansGluten += 1;
            }
            if (typesCook[i] === 'Thailandais') {
              this.typesCookPercentage.thailandais += 1;
            }
            if (typesCook[i] === 'Végan') {
              this.typesCookPercentage.vegan += 1;
            }
            if (typesCook[i] === 'Végétarien') {
              this.typesCookPercentage.vegetarien += 1;
            }
            if (typesCook[i] === 'Vietnamiem') {
              this.typesCookPercentage.vietnamiem += 1;
            }
          }
        }
        this.map = L.map('map', {
            scrollWheelZoom: false,
            zoomControl: false,
            center: [ 39.8282, -98.5795 ],
            zoom: 3
          });

        this.osmLayer = new L.TileLayer(
            'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
              minZoom: 1,
              maxZoom: 20,
              attribution: 'Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors'
            }
          );
        L.marker([this.restaurant.lat, this.restaurant.long], this.icon).addTo(this.map);
        this.map.setView(new L.LatLng(this.restaurant.lat, this.restaurant.long), 15);
        this.map.addLayer(this.osmLayer);
      }
        );
  }

  private loadReviewsById(id) {
    this.reviewService.readRestaurantReviews({id})
      .pipe(first())
      .subscribe(rest => {
        this.reviews = rest;
      });
  }

  doFilter(event) {
    if ( event !== '') {
      this.searchBarService.doSearchAvisRestaurant(event, this.id)
        .pipe(first())
        .subscribe(
          data => {
            this.reviews = data;
          }
        );
    } else {
      this.loadReviewsById(this.id);
    }
  }
  onChangePage(pageOfItems: Array<any>) {
    // update current page of items
    this.pageOfItems = pageOfItems;
  }
}
