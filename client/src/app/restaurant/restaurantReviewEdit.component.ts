import {AfterContentInit, Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AlertService, AuthenticationService, RestaurantService, StreetsService, RatingGlobalService, ReviewService} from '../_services';
import {User, Review} from '../_models';
import {first, map, startWith} from 'rxjs/operators';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {Observable} from 'rxjs';
import {MatAutocomplete, MatAutocompleteSelectedEvent, MatChipInputEvent} from '@angular/material';




@Component({
  templateUrl: 'restaurantReviewEdit.component.html'
})
export class RestaurantReviewEditComponent implements OnInit, AfterContentInit {
  currentUser: User;
  addReviewForm: FormGroup;
  loading = false;
  submitted = false;
  restaurant;
  id;
  ratingGlobal = '0';
  ratingValueForMoney = '0';

  visible = true;
  selectable = true;
  removable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  typeFoodCtrl = new FormControl();
  filteredTypeFood: Observable<string[]>;
  typeFoodSelected: string[] = [];
  allTypeFood: string[] = [
    'Américain',
    'Chinois',
    'Français',
    'Italien',
    'Japonais',
    'Mexicain',
    'Sans Gluten',
    'Thailandais',
    'Végan',
    'Végétarien',
    'Vietnamiem'
  ];
  @ViewChild('typeFoodInput',  {static: false}) typeFoodInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto',  {static: false}) matAutocomplete: MatAutocomplete;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private restaurantService: RestaurantService,
    private alertService: AlertService,
    private streetsService: StreetsService,
    private ratingService: RatingGlobalService,
    private reviewService: ReviewService
  ) {
    this.currentUser = this.authenticationService.currentUserValue;
    this.filteredTypeFood = this.typeFoodCtrl.valueChanges.pipe(
      startWith(null),
      map((typeFood: string | null) => typeFood ? this._filterTypeFood(typeFood) : this.allTypeFood.slice()));
  }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.addReviewForm = this.formBuilder.group({
      titre: ['', Validators.required],
      avis: ['', Validators.required],
      username: [this.authenticationService.currentUserValue.username],
      date: [new Date()],
      type: ['', Validators.required]
    });
    this.loadRestaurantById(this.id);

    this.ratingService.currentGlobalRating.subscribe(rating => {
      this.ratingGlobal = rating;
    });

    this.ratingService.currentValueForMoneyRating.subscribe(rating => {
      this.ratingValueForMoney = rating;
    });

  }



  ngAfterContentInit() {
  }

  onSubmit() {
    const review = {
      idRestaurant : this.restaurant.id,
      restaurantName: this.restaurant.name,
      title: this.addReviewForm.value.titre,
      avis: this.addReviewForm.value.avis,
      createdDate: new Date(),
      createdBy: this.addReviewForm.value.username,
      score: parseInt(this.ratingGlobal, 10),
      mood: this.addReviewForm.value.type,
      price: parseInt(this.ratingValueForMoney, 10),
      typeFood : this.typeFoodSelected.join(' ')
    };
    this.submitted = true;

    // reset alerts on submit
    this.alertService.clear();

    // stop here if form is invalid
    if (this.addReviewForm.invalid) {
      return;
    }
    this.loading = true;
    this.reviewService.createReview(review)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Review Ajoute', true);
          this.router.navigate(['/restaurant/' + this.id]);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        }
      );
  }

  get f() {
    return this.addReviewForm.controls;
  }

  private loadRestaurantById(id) {
    this.restaurantService.getRestaurantById({ id })
      .pipe(first())
      .subscribe(rest => {
        this.restaurant = rest;
      });
  }

  addTypeFood(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    if ( value in this.allTypeFood || !(this.typeFoodSelected.includes(value))) {
      if ((value || '').trim()) {
        this.typeFoodSelected.push(value.trim());
      }

      // Reset the input value
      if (input) {
        input.value = '';
      }

      this.typeFoodCtrl.setValue(null);
    }
  }

  removeTypeFood(typeFood: string): void {
    const index = this.typeFoodSelected.indexOf(typeFood);

    if (index >= 0) {
      this.typeFoodSelected.splice(index, 1);
    }
  }

  selectedTypeFood(event: MatAutocompleteSelectedEvent): void {
    if ( !(this.typeFoodSelected.includes(event.option.viewValue )) ) {
      this.typeFoodSelected.push(event.option.viewValue);
      this.typeFoodInput.nativeElement.value = '';
      this.typeFoodCtrl.setValue(null);
    }
  }

  private _filterTypeFood(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.allTypeFood.filter(typeFood => typeFood.toLowerCase().indexOf(filterValue) === 0);
  }

}
