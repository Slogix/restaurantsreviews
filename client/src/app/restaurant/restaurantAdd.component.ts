import { Component, OnInit, AfterContentInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { first } from 'rxjs/operators';

import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

import { AlertService, RestaurantService, AuthenticationService, StreetsService  } from '../_services';

import * as villesJson2 from 'src/assets/france.json';

import * as L from 'leaflet';


@Component({
  templateUrl: 'restaurantAdd.component.html'
})
export class RestaurantAddComponent implements OnInit, AfterContentInit {
  addRestaurantForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  villes2: any = (villesJson2  as  any).default;
  villes3 = [];
  streets = [];
  streetsName = [];
  codePostal;
  filteredCities: Observable<string[]>;
  filteredStreets: Observable<string[]>;
  map;
  osmLayer;
  marker;
  icon = {
    icon: L.icon({
      iconSize: [ 26, 41 ],
      iconAnchor: [ 13, 41 ],
      // specify the path here
      iconUrl: 'https://unpkg.com/leaflet@1.4.0/dist/images/marker-icon.png',
      shadowUrl: 'https://unpkg.com/leaflet@1.4.0/dist/images/marker-shadow.png'
    })
  };
  latitude;
  longitude;
  streetIsEnable = 0;


  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authentication: AuthenticationService,
    private restaurantService: RestaurantService,
    private alertService: AlertService,
    private streetsService: StreetsService
  ) {}

  ngOnInit() {
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.villes2.length; i++) {
      this.villes3.push(this.villes2[i].Nom_commune + ', ' + this.villes2[i].Code_postal);
    }
    this.addRestaurantForm = this.formBuilder.group({
      name: ['', Validators.required],
      phoneNumber: ['', Validators.pattern('[0-9 ]{10}')],
      city: ['', Validators.required],
      street: [{value: '', disabled: true}, Validators.required],
      username: [this.authentication.currentUserValue.username],
      date: [new Date()],
      lat: '',
      long: ''
    });
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.setStreetEnable();
  }

  ngAfterContentInit() {
    this.filteredCities = this.addRestaurantForm.get('city').valueChanges
      .pipe(
        startWith(''),
        map(value => this._filterCities(value))
      );

    this.filteredStreets = this.addRestaurantForm.get('street').valueChanges
      .pipe(
        startWith(''),
        map(value => this._filterStreets(value))
      );

    this.map = L.map('map', {
      scrollWheelZoom: true,
      zoomControl: true,
      center: [ 439.8282, -98.5795 ],
      zoom: 3
    });

    this.osmLayer = new L.TileLayer(
      'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        minZoom: 1,
        maxZoom: 20,
        attribution: 'Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors'
      }
    );

    this.map.setView(new L.LatLng(0, 0), 1);
    this.map.addLayer(this.osmLayer);

  }

  get f() {
    return this.addRestaurantForm.controls;
  }

  onSubmit() {
    // console.log(this.addRestaurantForm.value);
    this.submitted = true;

    // reset alerts on submit
    this.alertService.clear();

    // stop here if form is invalid
    if (this.addRestaurantForm.invalid) {
      return;
    }

    this.loading = true;

    this.restaurantService.createRestaurant(this.addRestaurantForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Restaurant Ajoute', true);
          this.router.navigate(['/']);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        }
      );
  }

  private _filterCities(value: string): string[] {
    const filterValue = value.toLowerCase();
    if (value.length > 1) {
      return this.villes3.filter(option => option.toLowerCase().startsWith(filterValue));
    } else {
      return [];
    }
  }

  private _filterStreets(value: string): string[] {
    const filterValue = value.toLowerCase();
    let streetsSplit = [];
    let streetsText = '';
    if (value.length > 0) {
      this.streetsService.getStreets(value, this.codePostal).subscribe((data) => {
        this.streets = data['features'];
        // console.log(this.streets);
        this.streetsName = [];
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < this.streets.length; i++) {
          streetsSplit = this.streets[i].properties.label.split(' ');
          // tslint:disable-next-line:prefer-for-of
          for (let j = 0; j < streetsSplit.length - 2; j++) {
            streetsText += streetsSplit[j] + ' ';
          }
          this.streetsName.push(streetsText);
          streetsText = '';
        }
        // console.log(this.streetsName);
        // console.log('a ' + this.streetsName.filter(option => option.toLowerCase().startsWith(filterValue)));
      });
      return this.streetsName.filter(option => option.toLowerCase().startsWith(filterValue));
    } else {
      return [];
    }
  }

  private changeLatLngCities(option: string) {
    const optionSplit = option.split(', ');
    const commune = optionSplit[0];
    const code = optionSplit[1];
    this.codePostal = code;
    let coord;
    let coord1;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.villes2.length; i++) {
      if ((this.villes2[i].Nom_commune === commune) && (this.villes2[i].Code_postal.toString() === code)) {
        coord = this.villes2[i].coordonnees_gps;
        coord1 = coord.split(', ');
        this.map.setView(new L.LatLng(coord1[0], coord1[1]), 11);
        this.map.addLayer(this.osmLayer);
      }
    }
    // console.log('ok');
    // this.addRestaurantForm.get('street').enable();
    this.streetIsEnable = 1;
  }

  private changeLatLngStreets(option: string) {
    let d;
    if (this.marker !== undefined) {
      this.map.removeLayer(this.marker);
    }
    this.streetsService.getStreets(option, this.codePostal).subscribe((data) => {
      d = data['features'];
      this.latitude = d[0].geometry.coordinates[1];
      this.longitude = d[0].geometry.coordinates[0];
      this.marker = L.marker([this.latitude, this.longitude], this.icon).addTo(this.map);
      this.addRestaurantForm.get('lat').setValue(this.latitude.toString());
      this.addRestaurantForm.get('long').setValue(this.longitude.toString());
      this.map.setView(new L.LatLng(this.latitude, this.longitude), 15);
      this.map.addLayer(this.osmLayer);
    });
  }

  private setStreetEnable() {
    const cityControl = this.addRestaurantForm.get('city');
    const streetControl = this.addRestaurantForm.get('street');

    cityControl.valueChanges.subscribe(city => {
      if (city !== '' && this.streetIsEnable === 1) {
        // console.log('city not empty');
        this.addRestaurantForm.get('street').enable();
      } else {
        // console.log('city is empty');
        this.streetIsEnable = 0;
        this.addRestaurantForm.get('street').disable();
      }
    });
  }
}

