/* tslint:disable:no-input-rename */
import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
// import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-mat-star-rating',
  templateUrl: './star-ratingGlobal.component.html'
  // encapsulation: ViewEncapsulation.Emulated
})
export class StarRatingGlobalComponent implements OnInit {

  @Input('rating') private rating;
  @Input('starCount') private starCount;
  @Input('color') private color;
  @Input('click') private click;
  @Output() private ratingUpdated = new EventEmitter();

  // private snackBarDuration = 2000;
  private ratingArr = [];
  msg = '';

  constructor() {
  }


  ngOnInit() {
    for (let index = 0; index < this.starCount; index++) {
      this.ratingArr.push(index);
    }
  }
  onClick(rating: number) {
    if (this.click) {
      this.ratingUpdated.emit(rating);
      return false;
    }
    return false;
  }

  showIcon(index: number) {
    if (this.rating >= index + 1) {
      return 'star';
    } else {
      return 'star_border';
    }
  }

  message(i: number) {
    switch (i) {
      case 1:
        this.msg = 'Horrible';
        break;
      case 2:
        this.msg = 'Médiocre';
        break;
      case 3:
        this.msg = 'Moyen';
        break;
      case 4:
        this.msg = 'Très bien';
        break;
      case 5:
        this.msg = 'Excellent';
        break;
    }
    return this.msg;
  }

}
export enum StarRatingColor {
  primary = 'primary',
  accent = 'accent',
  warn = 'warn'
}
