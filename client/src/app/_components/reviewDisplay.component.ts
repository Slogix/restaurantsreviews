import {Component, Input} from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr, 'fr');

@Component({
  selector: 'app-review-display',
  templateUrl: './reviewDisplay.component.html'
  // encapsulation: ViewEncapsulation.Emulated
})
export class ReviewDisplayComponent  {
  @Input('review') private review;
  @Input('restaurantName') private restaurantName;
  @Input('restaurantID') private restaurantID;

}

