import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService } from '../_services';
import { FormControl, FormGroup } from '@angular/forms';

@Component({ selector: 'app-search-bar', templateUrl: 'search-bar.component.html' })
export class SearchBarComponent implements OnInit {
  doSearch: FormGroup;
  loading = false;
  value = 'Restaurants';
  type: string [] = [
    'Restaurants',
    'Avis'
  ];
  constructor(
    private alertService: AlertService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.doSearch = new FormGroup({
      recherche: new FormControl(),
      type: new FormControl()
    });

  }
  onSubmit() {
    const query = this.doSearch.value;
    this.loading = true;
    this.router.navigate(['/search'], { queryParams: query} );

  }
}
