import { Component, OnInit, Input } from '@angular/core';
// import { JsonPipe } from '@angular/common';
import { StarRatingColor } from './star-ratingGlobal.component';
import { RatingGlobalService } from '../_services';

@Component({
  selector: 'app-star-global',
  templateUrl: './starGlobal.component.html',
})
export class StarGlobalComponent implements OnInit {
  @Input('rating') private rating;
  @Input('click') private click;
  starCount = 5;
  starColor: StarRatingColor = StarRatingColor.accent;
  starColorP: StarRatingColor = StarRatingColor.primary;
  starColorW: StarRatingColor = StarRatingColor.warn;

  constructor(private ratingService: RatingGlobalService) { }

  ngOnInit() {
  }
  onRatingChanged($event) {
    this.rating = $event;
    this.newRating($event);
  }

  newRating(rating: string) {
    this.ratingService.changeGlobalRating(rating);
  }



}
