import { Component, OnInit, Input } from '@angular/core';
// import { JsonPipe } from '@angular/common';
import { StarRatingValueForMoneyColor } from './star-ratingValueForMoney.component';
import { RatingGlobalService } from '../_services';

@Component({
  selector: 'app-star-valueForMoney-root',
  templateUrl: './starValueForMoney.component.html',
})
export class StarValueForMoneyComponent implements OnInit {
  @Input('rating') private rating;
  @Input('click') private click;
  starCount = 5;
  starColor: StarRatingValueForMoneyColor = StarRatingValueForMoneyColor.accent;
  starColorP: StarRatingValueForMoneyColor = StarRatingValueForMoneyColor.primary;
  starColorW: StarRatingValueForMoneyColor = StarRatingValueForMoneyColor.warn;

  constructor(private ratingService: RatingGlobalService) { }

  ngOnInit() {
  }
  onRatingChanged($event) {
    this.rating = $event;
    this.newRating($event);
  }

  newRating(rating: string) {
    this.ratingService.changeValueForMoneyRating(rating);
  }
}
