import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class RatingGlobalService {

  private ratingGlobalSource = new BehaviorSubject('0');
  currentGlobalRating = this.ratingGlobalSource.asObservable();

  private ratingValueForMoneySource = new BehaviorSubject('0');
  currentValueForMoneyRating = this.ratingValueForMoneySource.asObservable();

  constructor() { }

  changeGlobalRating(rating: string) {
    this.ratingGlobalSource.next(rating);
  }

  changeValueForMoneyRating(rating: string) {
    this.ratingValueForMoneySource.next(rating);
  }



}
