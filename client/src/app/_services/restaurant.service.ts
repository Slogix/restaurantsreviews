import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Restaurant } from '../_models';

@Injectable({ providedIn: 'root' })
export class RestaurantService {
  private apiUrl;
  constructor(private http: HttpClient) {
      this.apiUrl = 'http://localhost:4000';
    }

    getLastTen() {
        return this.http.get<Restaurant[]>(`${this.apiUrl}/restaurants/readLastTen`);
    }

    createRestaurant(restaurant: Restaurant) {
        return this.http.post(`${this.apiUrl}/restaurants/createRestaurant`, restaurant);
    }

    getRestaurantById(id) {
      return this.http.post<Restaurant>(`${this.apiUrl}/restaurants/readOne`, id);
    }

    /*delete(id: number) {
        return this.http.delete(`${config.apiUrl}/users/${id}`);
    }*/
}
