import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StreetsService {

  constructor(private httpClient: HttpClient) { }

  public getStreets(value: string, codePostal: string) {
    if (codePostal === undefined) {
      codePostal = '';
    }
    const s = value.split(' ');
    let streetFormated = '';
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < s.length; i++) {
      streetFormated = streetFormated + s[i] + '%20';
    }
    return this.httpClient.get('https://api-adresse.data.gouv.fr/search/?q=' +
      streetFormated + codePostal + '&type=housenumber&autocomplete=1');
  }
}
