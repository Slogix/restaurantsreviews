import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Review } from '../_models';

@Injectable({ providedIn: 'root' })
export class ReviewService {
  private apiUrl;
  constructor(private http: HttpClient) {
    this.apiUrl = 'http://localhost:4000';
  }

  getLastTen() {
    return this.http.get<Review[]>(`${this.apiUrl}/reviews/readLastTen`);
  }

  createReview(review: Review) {
    return this.http.post(`${this.apiUrl}/reviews/createReview`, review);
  }

  readRestaurantReviews(id) {
    return this.http.post(`${this.apiUrl}/reviews/readRestaurantReviews`, id);
  }
}
