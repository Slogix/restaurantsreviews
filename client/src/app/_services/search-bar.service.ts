import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Restaurant, Review} from '../_models';

@Injectable({ providedIn: 'root' })
export class SearchBarService {
  private apiUrl;

  constructor(private router: Router, private http: HttpClient) {
    this.apiUrl = 'http://localhost:4000';
  }
  doSearchRestaurant(search, type) {
    return this.http.post<Restaurant[]>(`${this.apiUrl}/restaurants/searchRestaurant`, {q: search, type: type});

  }
  doSearchAvis(search, type) {
    return this.http.post<Review[]>(`${this.apiUrl}/reviews/searchReview`, {q: search, type: type});
  }
  doSearchAvisRestaurant(search, id) {
    return this.http.post<Review[]>(`${this.apiUrl}/reviews/searchReviewRestaurant`, {q: search, id: id});
  }
  doFilterRestaurants({search, long, lat, distance, typeFoodSelected, note, ambiance, price}) {
    return this.http.post<Restaurant[]>(`${this.apiUrl}/restaurants/doFilter`, {
      q: search,
      typeFood: typeFoodSelected,
      long: long,
      lat: lat,
      distance: distance,
      note: note,
      ambiance: ambiance,
      price: price});
  }

  doFilterReviews({search, dateDebut, dateFin, note, ambiance, price}) {
    return this.http.post<Review[]>(`${this.apiUrl}/reviews/doFilter`, {
      q: search,
      note: note,
      dateDebut: dateDebut,
      dateFin: dateFin,
      ambiance: ambiance,
      price: price});
  }

}
