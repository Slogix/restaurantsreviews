﻿import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { SearchComponent } from './search';
import { AuthGuard } from './_helpers';
import {RestaurantAddComponent, RestaurantReviewEditComponent} from './restaurant';
import { RestaurantDetailsComponent } from './restaurant';


const routes: Routes = [
    { path: '', component: HomeComponent},
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'restaurant', component: RestaurantAddComponent, canActivate: [AuthGuard] },
    { path: 'restaurant/:id', component: RestaurantDetailsComponent },
    { path: 'search', component: SearchComponent },
    { path: 'restaurant/:id/ReviewEdit', component: RestaurantReviewEditComponent, canActivate: [AuthGuard] },
    { path: '**', redirectTo: '' }
];

export const appRoutingModule = RouterModule.forRoot(routes);
