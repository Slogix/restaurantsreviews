export class Restaurant {
    id: number;
    name: string;
    phoneNumber?: string;
    city: string;
    street: string;
    createdDate: Date;
    createdBy: string;
    score?: number;
    price?: number;
    lat: string;
    long: string;
}
