export class Review {
  idRestaurant: number;
  restaurantName?: string;
  title: string;
  avis: string;
  createdDate: Date;
  createdBy: string;
  score: number;
  mood: number;
  price: number;
  typeFood: string;
}
