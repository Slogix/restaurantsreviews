﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { appRoutingModule } from './app.routing';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { AppComponent } from './app.component';
import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import {RestaurantFilterComponent, SearchComponent} from './search';
import {
  AlertComponent,
  SearchBarComponent,
  StarGlobalComponent,
  StarRatingGlobalComponent,
  StarValueForMoneyComponent,
  StarRatingValueForMoneyComponent,
  ReviewDisplayComponent
} from './_components';
import { RestaurantAddComponent } from './restaurant';
import { RestaurantDetailsComponent } from './restaurant';
import { RestaurantReviewEditComponent } from './restaurant';
import { RestaurantCardComponent} from './restaurantCard';
import {
  MatAutocompleteModule,
  MatChipsModule,
  MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatSelectModule,
  MatSidenavModule,
  MatButtonModule,
  MatTooltipModule, MatDatepickerModule, MatNativeDateModule, MAT_DATE_LOCALE, MatSliderModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { JwPaginationComponent } from 'jw-angular-pagination';
import {ReviewFilterComponent} from './search/searchFilterReview/reviewFilter.component';






@NgModule({
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    appRoutingModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    MatInputModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    FormsModule,
    MatMenuModule,
    MatIconModule,
    MatSelectModule,
    FlexLayoutModule,
    MatSidenavModule,
    MatDividerModule,
    MatChipsModule,
    MatButtonModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSliderModule
  ],
    declarations: [
        AppComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        AlertComponent,
        SearchBarComponent,
        RestaurantAddComponent,
        RestaurantDetailsComponent,
        SearchComponent,
        RestaurantFilterComponent,
        RestaurantReviewEditComponent,
        StarGlobalComponent,
        StarRatingGlobalComponent,
        StarValueForMoneyComponent,
        StarRatingValueForMoneyComponent,
        RestaurantCardComponent,
        JwPaginationComponent,
        ReviewDisplayComponent,
        ReviewFilterComponent
    ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    MatDatepickerModule,
    { provide: MAT_DATE_LOCALE, useValue: 'fr-FR' },


    // provider used to create fake backend

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
