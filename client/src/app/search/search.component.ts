import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { ActivatedRoute, Router} from '@angular/router';
import { SearchBarService, AlertService } from '../_services';
import {Restaurant, Review} from '../_models';
import {FormBuilder, FormControl} from '@angular/forms';
// import { restaurantFilter } from './searchFilterRestaurant/restaurantFilter.component';


@Component({
  templateUrl: 'search.component.html',
  styleUrls: ['search.component.css']
})
export class SearchComponent implements OnInit {
  query: string;
  type: string;
  restaurants: Restaurant[];
  reviews: Review[];
  loading = false;
  sortCtrl = new FormControl();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private searchBarService: SearchBarService,
    private alertService: AlertService,
    private fb: FormBuilder
  ) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.query = this.route.snapshot.queryParamMap.get('recherche');
      this.type = this.route.snapshot.queryParamMap.get('type');
      this.loading = true;
      if ( this.type === 'Restaurants' ) {
        this.searchBarService.doSearchRestaurant(this.query, this.type )
          .pipe(first())
          .subscribe(
            data => {
              this.loading = false;
              this.restaurants = data;
            },
            error => {
              this.alertService.error(error);
              this.loading = false;
            }
          );
      } else {
        this.searchBarService.doSearchAvis(this.query, this.type )
          .pipe(first())
          .subscribe(
            data => {
              this.loading = false;
              this.reviews = data;
            },
            error => {
              this.alertService.error(error);
              this.loading = false;
            }
          );
      }
    });
  }
  selectValueReview( newValue: any) {
    newValue.search = this.query;
    this.searchBarService.doFilterReviews(newValue)
      .pipe(first())
      .subscribe(
        data => {
          console.log(data);
          this.reviews = data;
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        }
      );
  }
  selectValueRestaurant( newValue: any) {
    newValue.search = this.query;
    newValue.typeFoodSelected = newValue.typeFoodSelected.join(' ');
    this.searchBarService.doFilterRestaurants(newValue)
      .pipe(first())
      .subscribe(
        data => {
          this.restaurants = data;
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        }
      );
  }
  sort(event) {
    if ( event === 'notePlus' ) {
      if ( this.type === 'Restaurants') {
        this.restaurants.sort((a, b) => b.score - a.score);
      } else {
        this.reviews.sort((a, b) => b.score - a.score);
      }
    } else if ( event === 'noteMoins') {
      if ( this.type === 'Restaurants') {
        this.restaurants.sort((a, b) => a.score - b.score);
      } else {
        this.reviews.sort((a, b) => a.score - b.score);
      }
    } else if ( event === 'recent') {
      if ( this.type === 'Restaurants') {
        // @ts-ignore
        this.restaurants.sort((a, b) => new Date(b.createdDate) - new Date(a.createdDate));
      } else {
        // @ts-ignore
        this.reviews.sort((a, b) => new Date(b.createdDate) - new Date(a.createdDate));
      }
    } else if ( event === 'moinsRecent') {
      if ( this.type === 'Restaurants') {
        // @ts-ignore
        this.restaurants.sort((a, b) => new Date(a.createdDate) - new Date(b.createdDate));
      } else {
        // @ts-ignore
        this.reviews.sort((a, b) => new Date(a.createdDate) - new Date(b.createdDate));
      }
    } else if ( event === 'qualitePlus') {
      if ( this.type === 'Restaurants') {
        this.restaurants.sort((a, b) => b.price - a.price);
      } else {
        this.reviews.sort((a, b) => b.price - a.price);
      }
    } else if ( event === 'qualiteMoins') {
      if ( this.type === 'Restaurants') {
        this.restaurants.sort((a, b) => a.price - b.price);
      } else {
        this.reviews.sort((a, b) => a.price - b.price);
      }
    }
  }
}
