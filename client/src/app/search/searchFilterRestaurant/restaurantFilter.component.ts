import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import { FormControl } from '@angular/forms';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {Observable} from 'rxjs';
import {MatAutocomplete, MatAutocompleteSelectedEvent, MatChipInputEvent} from '@angular/material';
import {map, startWith} from 'rxjs/operators';
import {RatingGlobalService} from '../../_services';

@Component(
  {selector: 'app-restaurant-filter',
    templateUrl: 'restaurantFilter.component.html',
    styleUrls: ['restaurantFilter.component.css']})

export class RestaurantFilterComponent implements OnInit {
  visible = true;
  selectable = true;
  removable = true;
  note;
  price;
  ambiance;
  lat;
  long;
  distanceFilter;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  typeFoodCtrl = new FormControl();
  ambianceCtrl = new FormControl();
  filteredTypeFood: Observable<string[]>;
  typeFoodSelected: string[] = [];
  allTypeFood: string[] = [
    'Américain',
    'Chinois',
    'Français',
    'Italien',
    'Japonais',
    'Mexicain',
    'Sans Gluten',
    'Thailandais',
    'Végan',
    'Végétarien',
    'Vietnamiem'
  ];
  // TODO S'occuper de l'affichage de l'ambiance dans la liste selected
  ambiances: string[] = [
    'amis',
    'couple',
    'famille',
    'affaires',
    'solo',
  ];

  @ViewChild('typeFoodInput',  {static: false}) typeFoodInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto',  {static: false}) matAutocomplete: MatAutocomplete;
  @Output() selectValue = new EventEmitter<{long: string, lat: string, distance: string,
    typeFoodSelected: string[], note: number, price: number, ambiance: string}>();
  ngOnInit() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition( location => {
        this.lat = location.coords.latitude;
        this.long = location.coords.longitude;
      });
    }
  }
  constructor(private ratingGlobalService: RatingGlobalService) {
    this.filteredTypeFood = this.typeFoodCtrl.valueChanges.pipe(
      startWith(null),
      map((typeFood: string | null) => typeFood ? this._filterTypeFood(typeFood) : this.allTypeFood.slice()));
    ratingGlobalService.currentGlobalRating.subscribe(rating => {
      this.note = rating;
      this.selectValue.emit({long: this.long, lat: this.lat, distance: this.distanceFilter,
        typeFoodSelected: this.typeFoodSelected, note: this.note,
        ambiance: this.ambiance, price: this.price});
    });
    ratingGlobalService.currentValueForMoneyRating.subscribe( rating => {
      this.price = rating;
      this.selectValue.emit({long: this.long, lat: this.lat, distance: this.distanceFilter,
        typeFoodSelected: this.typeFoodSelected, note: this.note,
        ambiance: this.ambiance, price: this.price});
    });
    this.ambianceCtrl.valueChanges.subscribe(ambiance => {
      this.ambiance = ambiance;
      this.selectValue.emit({long: this.long, lat: this.lat, distance: this.distanceFilter,
        typeFoodSelected: this.typeFoodSelected, note: this.note,
        ambiance: this.ambiance, price: this.price});
    });
  }
  distance(event: any) {
    this.distanceFilter = event;
    this.selectValue.emit({long: this.long, lat: this.lat, distance: this.distanceFilter,
      typeFoodSelected: this.typeFoodSelected, note: this.note,
      ambiance: this.ambiance, price: this.price});
  }
  addTypeFood(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    if ( value in this.allTypeFood || !(this.typeFoodSelected.includes(value))) {
      if ((value || '').trim()) {
        this.typeFoodSelected.push(value.trim());
        this.selectValue.emit({long: this.long, lat: this.lat, distance: this.distanceFilter,
          typeFoodSelected: this.typeFoodSelected, note: this.note,
          ambiance: this.ambiance, price: this.price});
      }
      if (input) {
        input.value = '';
      }
      this.typeFoodCtrl.setValue(null);
    }
  }

  removeTypeFood(typeFood: string): void {
    const index = this.typeFoodSelected.indexOf(typeFood);

    if (index >= 0) {
      this.typeFoodSelected.splice(index, 1);
      this.selectValue.emit({long: this.long, lat: this.lat, distance: this.distanceFilter,
        typeFoodSelected: this.typeFoodSelected, note: this.note,
        ambiance: this.ambiance, price: this.price});
    }
  }

  selectedTypeFood(event: MatAutocompleteSelectedEvent): void {
    if ( !(this.typeFoodSelected.includes(event.option.viewValue )) ) {
      this.typeFoodSelected.push(event.option.viewValue);
      this.selectValue.emit({long: this.long, lat: this.lat, distance: this.distanceFilter,
        typeFoodSelected: this.typeFoodSelected, note: this.note,
        ambiance: this.ambiance, price: this.price});
      this.typeFoodInput.nativeElement.value = '';
      this.typeFoodCtrl.setValue(null);
    }
  }

  formatLabel(value: number) {
    return value + 'km';
  }
  private _filterTypeFood(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.allTypeFood.filter(typeFood => typeFood.toLowerCase().indexOf(filterValue) === 0);
  }
}
