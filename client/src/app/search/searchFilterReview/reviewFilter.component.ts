import {Component, ElementRef, EventEmitter, Output, ViewChild} from '@angular/core';
import { FormControl } from '@angular/forms';
import {MatAutocomplete} from '@angular/material';
import {RatingGlobalService} from '../../_services';

@Component(
  {selector: 'app-review-filter',
    templateUrl: 'reviewFilter.component.html',
    styleUrls: ['reviewFilter.component.css']})

export class ReviewFilterComponent {
  visible = true;
  selectable = true;
  removable = true;
  note;
  price;
  dateDebut;
  dateFin;
  ambiance;
  ambianceCtrl = new FormControl();
  datePickerCtrl = new FormControl();
  datePickerCtrl2 = new FormControl();
  // TODO S'occuper de l'affichage de l'ambiance dans la liste selected
  ambiances: string[] = [
    'amis',
    'couple',
    'famille',
    'affaires',
    'solo'
  ];

  @ViewChild('typeFoodInput',  {static: false}) typeFoodInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto',  {static: false}) matAutocomplete: MatAutocomplete;
  @Output() selectValue = new EventEmitter<{dateFin: Date, dateDebut: Date, note: number, price: number, ambiance: string}>();
  constructor(private ratingGlobalService: RatingGlobalService) {
    ratingGlobalService.currentGlobalRating.subscribe(rating => {
      this.note = rating;
      this.selectValue.emit({ dateFin: this.dateFin, dateDebut: this.dateDebut,
        note: this.note, ambiance: this.ambiance, price: this.price});
    });
    ratingGlobalService.currentValueForMoneyRating.subscribe( rating => {
      this.price = rating;
      this.selectValue.emit({ dateFin: this.dateFin, dateDebut: this.dateDebut,
        note: this.note, ambiance: this.ambiance, price: this.price});
    });
    this.ambianceCtrl.valueChanges.subscribe(ambiance => {
      this.ambiance = ambiance;
      this.selectValue.emit({ dateFin: this.dateFin, dateDebut: this.dateDebut,
        note: this.note, ambiance: this.ambiance, price: this.price});
    });
    this.datePickerCtrl.valueChanges.subscribe( date => {
      this.dateDebut = date;
      this.selectValue.emit({ dateFin: this.dateFin, dateDebut: this.dateDebut,
        note: this.note, ambiance: this.ambiance, price: this.price});
    });
    this.datePickerCtrl2.valueChanges.subscribe( date => {
      this.dateFin = date;
      this.selectValue.emit({ dateFin: this.dateFin, dateDebut: this.dateDebut,
        note: this.note, ambiance: this.ambiance, price: this.price});
    });

  }
}
