﻿import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import {Restaurant, Review, User} from '../_models';
import { ReviewService, RestaurantService } from '../_services';

@Component({ templateUrl: 'home.component.html', styleUrls: ['home.component.css'] })
export class HomeComponent implements OnInit {
  currentUser: User;
  restaurants: Restaurant[];
  reviews: Review[];

  constructor(
    private restaurantService: RestaurantService,
    private reviewService: ReviewService
  ) {
  }

  ngOnInit() {
    this.loadRestaurants();
    this.loadReviews();
  }

  async loadRestaurants() {
    await this.restaurantService.getLastTen()
      .pipe(first())
      .subscribe( restaurants => this.restaurants = restaurants);
  }
  async loadReviews() {
    await this.reviewService.getLastTen()
      .pipe(first())
      .subscribe( reviews => this.reviews = reviews);
  }
}
